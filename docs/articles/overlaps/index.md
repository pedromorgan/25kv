Overlaps
=========================


https://twitter.com/25kV/status/1063922732855894016

Ever wondered how it is possible to run 
- a continuous wire from, say, London to Glasgow? 

## Well... it isn't.

(Thread) I shot some video earlier this week and Strictly is on, so... 
lets talk about overlaps.

<iframe width="560" height="315" src="https://www.youtube.com/embed/5nLwKRtfOKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 
## 400 miles / 650km


- Firstly, no-one is going to sell you 400 miles of cable on a drum.
- 2km is about the biggest drum (weight?)

![Drum](https://learninglegacy.crossrail.co.uk/wp-content/uploads/2018/07/7J-007-Figure-19a.jpg)

Couldn't you just splice the cables together? 

- Well yes, but consider expansion/contraction with temperature. 
- The thermal coefficient of Cu alloy is 1.7x10^-5. 
- To put it another way, for every 1°C change in temp, it changes length by 0.0017%.



Euston to Glasgow is 400 miles, bang on. So if you fixed the contact wire above 
the buffer stops at Euston and spliced all the way to Glasgow, with an operating 
temp range of -18°C to +38°C (standard in UK) you'd have

```
400 x 1609.3 x 56 x 1.7x10^-5 = 612.8m of movement at Glasgow
```

Dealing with that level of movement is impossible.
 
- In the height of summer the end would be at the buffer stops. 
- In winter, it would be somewhere south of the Clyde.

## What to do?

Well you have to split your wire into smaller lengths - `tension lengths`, each less than 2km long.

So how do you ensure the pantograph sees a continuous wire? 

It is essential to transfer the pantograph from tension length to tension length without 
any significant change in dynamics.

These transfer zones are called `overlaps`. Lets look at how they work.

## traditional overlap

This video (c)Atkins shows how a traditional overlap
 
- with anchors on masts 
- allows the pantograph to pass from one wire to the next without interruption. 
- The contact wire is carefully profiled to provide a zone of parallel running
- where both wires are in contact with the pan. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/5nLwKRtfOKs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 

Maybe that's a bit abstract - lets look at a real world example.

This week 
@networkrailwest
 and 
@GWRHelp
began running from Swindon to London on electric power 
for the first time. Here's some footage of one running through 
a high speed overlap - I've added some annotation.
 
## Observations

There are a number of interesting things going on there - (depending on how you calibrate interest). 

The wire is quite lively - that's because we are at a foot crossing, so the 
wire has been placed high for safety reasons.
 
Ideally pantographs would have a behaviour that was independent 
of wire height, but that isn't possible due to the increased 
aerodynamic drag from a fully extended pan. 

- At these wire heights the pan pushes harder on the wire
- and you get more uplift and movement.
 
Also, look at how the wire runs are 
managed in the video. Unlike in the animation, the new wire drops 
into running from above 

- Series 1 wire runs are usually anchored over the 
  track rather than at lineside, so they don't come in at at angle.
- This reduces hookover risk by keeping the wire away from the ends 
  of the pan, as well as making the anchors easier 
  to build and maintain using road-rail plant.

Also notice how the wave set up by the pan 

- reaches all the way to the anchor
- where it will reflect a portion back towards the train. 

## High Speed

On high speed lines this can become a problem, so the 
overlap takes place over two or more spans to reduce wave reflections.

You will find an overlap every 1200-1500m on modern spring-tensioned systems - or up to 1970m on older, weight tensioned systems.

If you're on an electric train, look up and watch them go past on the adjacent track. Guaranteed to send you to sleep.

Much like this thread. /ends

(Also, a big shout out to 
@shotcutapp
, whose incredible software I used to put the video together. It's like photoshop for video, but free.)

