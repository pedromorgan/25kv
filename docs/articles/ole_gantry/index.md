OLE Steel work
=========================

https://twitter.com/25kV/status/1063922732855894016

A thread about OLE systems, steelwork sizes and the 
general perception that we've got it wrong...

Ever wondered why so many different 
systems worldwide? After all there are only a handful of track forms.

Unlike track, OLE has two wobbly things interacting:

- the wire 
- and the pan (pantograph)




Getting that interaction right is critical 

- if you don't, the necessarily lightweight (read: fragile) pan will be damaged 
- or worse, the OLE will

The interaction is largely a function of two things: 

- the speed you want to go, 
- and the number and spacing of pans on each train


Lets take the simplest scenario: 

- a train with one pan. As the pan moves along the wire, 
- the upward force of the pan sets up a wave. 
- The wave travels along the wire. 
- If the pan catches up with its wave, BAD THINGS HAPPEN

That tension has to be resisted where wire run terminate on masts or booms. So 
higher speed systems need bigger sections to withstand the load OR more complex tie arrangements - which take longer to install

Now lets go back to our train. This time it is 3 x 4 car EMUs coupled together - so 3 pans at around 100 metre spacings, since 25000V plug couplers aren't currently practical or safe

Remember that wave? Bad news - it travels in both directions from each pan - forwards AND backwards. And when the wave from pan 1 gets to pan 2, MORE BAD THINGS HAPPEN

So multiple pan trains are harder to deal with - the more closely spaced the pans are, the worse the problem is

So what is the solution? Increase your tension again. Tension = stiffness = less uplift from the pan = smaller waves.

Modern trains are - rightly or wrongly, I'm not the traction engineer - being specified to run with 3 pans at 110mph, and 2 pans at 125mph

These factors conspire to increase the sections of SOME elements of an OLE system - specifically the ones that terminate those tensions at the end of wire runs

Another element is deflection. Deflection of OLE masts happens when it is windy, and is bad news because it moves the OLE closer to the end of the pan. This increases reliability and dewirement risk.

Older OLE systems (that beloved mark 3b) used 6 inch masts, which have a deflection under maximum wind that would not be acceptable on today's railway. The right size for a single track cantilever on straight track in a low wind area is 8 inches - you can quote me on that

Another element that drives steel size is construction strategy. The systems that are  lighter usually use triangles to create strength. This is basic good engineering.

But triangles have to be bolted together, usually on site.

Got a greenfield railway, with unfettered access - like HS1 or HS2? Great, use those triangles, take your time with the build.

Got 4 hours max midweek night access & 2 years to complete the build? You might want to replace the mast/boom triangle with a stronger direct connection

So in conclusion - next time you see a lightweight OLE system, maybe from another era or country, and you're tempted to claim our current systems are over-engineered because they use bigger steel, first ask yourself this:

Is this a high speed system, designed for multiple pans, and rapid installation with high reliability? Or is it a low speed system? Or for single pans only? Or built in the days when OLE behaviour was poorly understood and so less reliable? Or was it built in a greenfield?

